#!/bin/bash

rustc vm.rs -A warnings


if [ $# -eq 1 ]; then
  ./vm  "$1"
  exit 1
fi
if [ $# -eq 0 ]; then
  echo "Just Compiling..."
  exit 0
fi

echo "Error! Too many arguments."
exit 2