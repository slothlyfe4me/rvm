use std::io::stdin;
use std::env;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::io::BufReader;


fn main() 
{
    let args: Vec<String> = env::args().collect();
    //println!("My path is {}.", args[0]);
    //println!("I got {:?} arguments: {:?}", args.len() - 1, &args[1..]);

    match args.len(){
        2 => {
                runfile(&args[1]);
            },
        _ => println!("ERROR - 1 Argument Required."),
    }
}


fn runfile(filename: &str) -> i32
{
    let path = Path::new(&filename);
    let display = path.display(); 
    println!("Opening file {}...", &display);

    let mut file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that
        // describes the error
        Err(why) => 
        {
            panic!("\n\n++++++++++++++++++++++++\nOpening File Error\nFile: {}\nError: {}\n++++++++++++++++++++++++\n\n", display, why.description());
            return 1;
        },
        Ok(file) => file,
    };

    let f = BufReader::new(file);
    let mut lnumber = 1;

    for line in f.lines() {

        let mut uw = line.unwrap();
        let mut iter = uw.split_whitespace();

        'endofline: loop
        {
            let mut temp = iter.next();
            match temp
            {
                Some(x) =>  
                {
                    passone(&lnumber, &x, &iter);
                },
                None => 
                {
                    lnumber = lnumber + 1;
                    println!();
                    break 'endofline;
                },
            }
        }



    }


    return 0;

}


fn isType(val: &str) -> bool
{
    match val{
        ".INT" | ".UINT" | ".FLOAT" | ".DOUBLE" | ".LONG" | ".BYT" =>  true,
        _ => false,
    }
}

fn isOpcode(val: &str) -> bool
{
    match val{
        "JMP" | "JMR" | "BNZ" | "BGT" | "BLT" | "BRZ" => true,
        "MOV_INT" | "LDA_INT" | "STR_INT" | "LDR_INT" | "STB" | "LDB" => true,
        "ADD_INT" | "ADI_INT" | "SUB_INT" | "MUL_INT" | "DIV_INT" |"AND_INT" | "OR_INT" | "CMP_INT" | "POW_INT" | "MOD_INT"   =>  true,
        "TRP" => true,
        _ => false,
    }
}

fn getOpcodeVal(val: &str) -> i32
{
    match val{
        "JMP" => 1,
        "JMR" => 2, 
        "BNZ" => 3,
        "BGT" => 4,
        "BLT" => 5, 
        "BRZ" => 6,
        "TRP" => 7,

        "MOV_INT" => 20,
        "LDA_INT" => 21,
        "STR_INT" => 22,
        "LDR_INT" => 23,
        "STB" => 24,
        "LDB" => 25,

        "ADD_INT" => 100,
        "ADI_INT" => 101,
        "SUB_INT" => 102,
        "MUL_INT" => 103,
        "DIV_INT" => 104,
        "POW_INT" => 105,
        "MOD_INT" => 106,
        "AND_INT" => 107,
        "OR_INT" => 108,
        "CMP_INT" => 109,

        _ => 0,
    }
}


//Create Symbol Table
fn passone(ln: &i32, v1: &str, iter: &std::str::SplitWhitespace<'_>) {

    //if not, then check if isType. If it is, increase it by the size of the Type, and then check to make sure flag is set to true. If false then throw an error.
    //if not, then check if isOpcode. If it is increase it by the size of the Opcode, and then check to make sure flag is set to false. If not, set it to false.

    //if first one is a label (not a type, not an opcode), then save the current pc
        //then see if the next is a Type. If it is, increase it by the size of the Type, append ("label", saved_PC) and then check to make sure flag is set to true. If false then throw and error.
        //if opcode, then increase size by opcode_instruction_size, append ("label", saved_PC), and then check to make sure flag is set to false. If not set it to false.
    print!("{}:{}\t", ln, v1);
}



//1, write integer to standard out
//2, read an integer from standard in
//3, write single character to standard out
//4, read a single character from standard in
//Read or write a value from register R3.