//Compiled Project aaaa.asm by Edison Earnest

//Calling Globals
S100	.INT	0		//null
S101	.INT	0		//false
S102	.INT	1		//true
N103	.INT	+1
N104	.INT	+4
N108	.INT	+0
H119	.BYT	10

OVERFLOW		.INT	-9999
UNDERFLOW		.INT	-9998

		ADI			R1	1
		MOV		R5		SP				//START FRAME: 
		ADI		R5		-24
		CMP		R5		SL
		BLT		R5		OVERFLOW
		MOV		R3		FP
		MOV		R6		SP
		ADI		SP		-4
		STR		R3		SP
		ADI		SP		-4
		STR		R0		SP
		ADI		SP		-4

		MOV		FP		R6
		ADI		SP		-12
		MOV		R2		PC				//START CALL	
		ADI		R2		36
		STR		R2		FP
		JMP		F117

		TRP		0						//31:	}

M106	ADI		R0		0				//5:	public void ttt()
		LDR		R2		N108			//7:	int i = 0;
		MOV		R3		FP			
		ADI		R3		-12
		STR		R2		R3
BEGIN0	MOV		R2		FP			//8:	while( i < 4)
		ADI		R2		-12
		LDR		R2		R2
		LDR		R3		N104			//
		CMP		R2		R3
		BLT		R2		L0
		MOV		R4		R0
		JMP		L1
L0	MOV		R4		R1
L1	MOV		R3		FP			
		ADI		R3		-16
		STR		R4		R3
		MOV		R2		FP			//
		ADI		R2		-16
		LDR		R2		R2
		BRZ		R2		ENDWHILE1
		MOV		R2		FP			//10:	i = i + 1;
		ADI		R2		-12
		LDR		R2		R2
		LDR		R3		N103			//
		ADD		R2		R3
		MOV		R3		FP			
		ADI		R3		-20
		STR		R2		R3
		MOV		R2		FP			//
		ADI		R2		-20
		LDR		R2		R2
		MOV		R3		FP			
		ADI		R3		-12
		STR		R2		R3
			JMP		BEGIN0		//12:	}


ENDWHILE1	MOV		SP		FP				//RTN	
		MOV		R5		SP
		CMP		R5		SB
		BGT		R5		UNDERFLOW
		LDR		R5		FP
		MOV		R6		FP
		ADI		R6		-4
		LDR		FP		R6
		JMR		R5

X111	ADI		R0		0				//14:	test2()
		MOV		R5		SP				//START FRAME: 
		ADI		R5		-12
		CMP		R5		SL
		BLT		R5		OVERFLOW
		MOV		R3		FP
		MOV		R6		SP
		ADI		SP		-4
		STR		R3		SP
		ADI		SP		-4
		MOV		R3		FP
		ADI		R3		-8
		LDR		R3		R3
		STR		R3		SP
		ADI		SP		-4

		MOV		FP		R6
		MOV		R2		PC				//START CALL	
		ADI		R2		36
		STR		R2		FP
		JMP		Z112

		LDR		R2		N108			//16:	int i = 0;
		MOV		R3		FP			
		ADI		R3		-12
		STR		R2		R3
BEGIN2	MOV		R2		FP			//17:	while( i < 4)
		ADI		R2		-12
		LDR		R2		R2
		LDR		R3		N104			//
		CMP		R2		R3
		BLT		R2		L2
		MOV		R4		R0
		JMP		L3
L2	MOV		R4		R1
L3	MOV		R3		FP			
		ADI		R3		-16
		STR		R4		R3
		MOV		R2		FP			//
		ADI		R2		-16
		LDR		R2		R2
		BRZ		R2		ENDWHILE3
		MOV		R2		FP			//19:	i = i + 1;
		ADI		R2		-12
		LDR		R2		R2
		LDR		R3		N103			//
		ADD		R2		R3
		MOV		R3		FP			
		ADI		R3		-20
		STR		R2		R3
		MOV		R2		FP			//
		ADI		R2		-20
		LDR		R2		R2
		MOV		R3		FP			
		ADI		R3		-12
		STR		R2		R3
			JMP		BEGIN2		//21:	}


ENDWHILE3	MOV		R2		FP			//RETURN this: 	22:	}
		ADI		R2		-8
		LDR		R2		R2
		MOV		SP		FP
		MOV		R5		SP
		CMP		R5		SB
		BGT		R5		UNDERFLOW
		LDR		R5		FP
		MOV		R6		FP
		ADI		R6		-4
		LDR		FP		R6
		STR		R2		SP
		JMR		R5

Z112	ADI		R0		0				//


		MOV		SP		FP				//RTN	
		MOV		R5		SP
		CMP		R5		SB
		BGT		R5		UNDERFLOW
		LDR		R5		FP
		MOV		R6		FP
		ADI		R6		-4
		LDR		FP		R6
		JMR		R5

F117	ADI		R0		0				//25:	void kxi2019 main() {


		MOV		R3		SL				//NEWI	27:	test2 a = new test2();
		MOV		R2		R3
		ADI		R3		0
		MOV		SL		R3
		MOV		R3		FP			
		ADI		R3		-16
		STR		R2		R3
		MOV		R5		SP				//START FRAME: 
		ADI		R5		-24
		CMP		R5		SL
		BLT		R5		OVERFLOW
		MOV		R3		FP
		MOV		R6		SP
		ADI		SP		-4
		STR		R3		SP
		ADI		SP		-4
		MOV		R3		FP			//
		ADI		R3		-16
		LDR		R3		R3
		STR		R3		SP
		ADI		SP		-4

		MOV		FP		R6
		ADI		SP		-12
		MOV		R2		PC				//START CALL	
		ADI		R2		36
		STR		R2		FP
		JMP		X111



		MOV		R2		SP				//PEEK	
		LDR		R2		R2
		MOV		R3		FP			
		ADI		R3		-20
		STR		R2		R3
		MOV		R2		FP			//
		ADI		R2		-20
		LDR		R2		R2
		MOV		R3		FP			
		ADI		R3		-12
		STR		R2		R3
		LDB		R3		H119			//30:	cout << '\n';
		TRP		3


		MOV		SP		FP				//RTN	
		MOV		R5		SP
		CMP		R5		SB
		BGT		R5		UNDERFLOW
		LDR		R5		FP
		MOV		R6		FP
		ADI		R6		-4
		LDR		FP		R6
		JMR		R5

