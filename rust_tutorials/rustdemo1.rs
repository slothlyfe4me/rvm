use std::{i8, i16, i32, i64, u8, u16, u32, u64, isize, usize, f32, f64};

use std::io::stdin;

fn main()
{
    println!("Hello World");
    let num = 10;
    let mut age: i32 = 40;

    println!("Max i8 {}", i8::MAX);
    println!("Min i8 {}", i8::MIN);


    println!("Max i16 {}", i16::MAX);
    println!("Min i16 {}", i16::MIN);

    
    println!("Max u16 {}", u16::MAX);
    println!("Min u16 {}", u16::MIN);


    let is_true: bool = true;

    let lex_x: char = 'x';

    println!("I am {} years old", num);
}
