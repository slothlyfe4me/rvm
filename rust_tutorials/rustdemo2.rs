use std::io::stdin;


fn main()
{
    let rand_string = "I am a random string";

    println!("Length : {}", rand_string.len());

    let (first, second) = rand_string.split_at(6);
    println!("First : {} Second : {}", first, second);


    let count = rand_string.chars().count();
    let mut chars = rand_string.chars();

    let mut individ_char = chars.next();



    loop
    {
        match individ_char{
            Some(x) => println!("{}", x),
            None => break,

        }
        individ_char = chars.next();


    }



    let mut iter = rand_string.split_whitespace();

    let mut individ_word = iter.next();

    loop {

        match individ_word{
            Some(x) => println!("{}", x),
            None => break,

        }

        individ_word = iter.next();
    }


    let rand_string2 = "I am a random string\nThere are other strings like it \n This string is the best";

    let mut lines = rand_string2.lines();
    let mut individ_line = lines.next();

    loop{
        match individ_line{
            Some(x) => println!("{}", x),
            None => break,

        }
        individ_line = lines.next();
    }


    println!("Find Best : {}", rand_string2.contains("best"));


}
